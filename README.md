# README #

![canvasTimeline.png](https://bitbucket.org/repo/7Ezp6AX/images/3875394914-canvasTimeline.png)

### What is this repository for? ###

* CanvasTimeline "plugin" test assignment.

### How do I get set up? ###

* Open index.html to view
* reference the canvasTimeline.Js in your Html file and call the function canvasTimeline.init(periodInSeconds)

### Created by: ###

* Aleksei Maide